/***************************************************************************
# Parsing function for EWB monitor debug frames
# 
# Morten P. Møller, March 2023.
#
#***************************************************************************/
#ifndef Parser_H
#define Parser_H

/***************************************************************************/
/* parameters */

/***************************************************************************/
/* variables */
//char *DebugFrameDummy = "VFS 254.32 347 Cnt 999 Starts 10 WM 12312 Air 250 Bat 120";
extern char *Delimiter;
extern char *vfs_vol_l;
extern char *vfs_vol_ul;
extern char *vfs_temp;
extern char *vfs_flow_cnt;
extern char *vfs_start_cnt;
extern char *wmpulse_cnt;
extern char *temp1;
extern char *batt_v;

/***************************************************************************/
/* function prototypes */
void InitializeParser(void);
void SendFrameRequest(void);
bool ReadCharToStr(void);
String GetString(void);
void ClearString(void);
bool IdentifyFrame(void);
void ParseFrame(char *DebugFrame);

#endif
/***************************************************************************/
