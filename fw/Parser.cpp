/***************************************************************************
# Parsing function for EWB monitor debug frames
#
# Morten P. Møller, march 2023
#
#****************************************************************************
Revision
2023-03-16 MPM: First draft
2023-08-18 MPM: Adaptation to new HW platform (frame format)

****************************************************************************/
/* includes */
#include <Arduino.h>
#include <stddef.h>
#include <string.h>
#include <HardwareSerial.h>
#include "Parser.hpp"
#include "Global.hpp"

/***************************************************************************/
/* parameters */

/***************************************************************************/
/* defines */

/***************************************************************************/
/* variables */
String InputStr = "";
String DebugFrameID = "$UPD";
String FrameID = "";      // First 4 chars of a frame
String RequestStr = "$R"; // Request string to Monitor

char *Delimiter;          // Space character
char *vfs_vol_l;          // Volume (l)
char *vfs_vol_ul;         // Volume (l/100)
char *vfs_temp;           // Temperature (deg C * 10)
char *vfs_flow_cnt;       // Flow counter
char *vfs_start_cnt;      // Flow start counter
char *wmpulse_cnt;        // Pulse counter
char *temp1;              // Temperature (deg C * 10)
char *batt_v;             // Battery voltage (V * 10)

/****************************************************************************
* Initialize serial port for receiving debug frames                 		  
*****************************************************************************/
void InitializeParser() 
{
  Delimiter = ",";
  InputStr.reserve(50);     // Allocate 50 chars
  Serial.begin(115200);     // initialize UART with baud rate of 115.200 bps
//  Serial.print("EWB Monitor - Display Box, ");
//  Serial.println(FW_VERSION); 

  vfs_vol_l = "?";          // Default values
  vfs_vol_ul = "?";
  vfs_temp = "?";
  vfs_flow_cnt = "?";
  vfs_start_cnt = "?";
  wmpulse_cnt = "?";
  temp1 = "?";
  batt_v = "?";
}

/****************************************************************************
* Send request to IWB Monitor for a new telegram.                		                     		  
*****************************************************************************/
void SendFrameRequest()
{
  Serial.print(RequestStr);
}

/****************************************************************************
* Reads chars from serial port until LF is received.                		  
* Return 1 if LF is received, otherwise 0                           		  
*****************************************************************************/
bool ReadCharToStr() 
{
  if (Serial.available())   // Check if a char is received
  {
    char tmpChar = Serial.read();
    if (tmpChar > 0)
    {
      if (tmpChar == 13)    // Check if CR is received -> Frame is complete
      {
        Serial.end();       // Pause RX while processing frame
        return 1;
      }
      InputStr += tmpChar;  // Append to existing string
      if (InputStr.length() > 49)
        InputStr = "";      // Avoid overrun of string
    }
//    Serial.print(InputStr);
  }
  return 0;
}

/****************************************************************************
* Returns the complete string                                             
* Returns String   (no CR or LF necessary)                                  		  
*****************************************************************************/
String GetString()
{
  String tmp;
//  return (InputStr);// + "\n\r");
  tmp = InputStr;
  Serial.begin(115200);     // Restart RX
  return tmp;
}
 
 /***************************************************************************
* Empties the temporary string for new serial input capturing             
*****************************************************************************/
void ClearString()
{
  InputStr = "";
}

 /***************************************************************************
* Checks if the frame received is a debug frame
* Return TRUE if the first 4 chars = "$UPD", else FALSE             
*****************************************************************************/
bool IdentifyFrame(void)
{
  FrameID = InputStr.substring(0,4);  // Get the first 4 chars

  if (FrameID.equals(DebugFrameID))
    return 1;
  else
    return 0;
}

/****************************************************************************
* Searches the total frame for individual values/data                		   
* Format:   UPD,liters,temp,count,starts,wm,air,bat
*        
* Example: $UPD,1792.38,667,11542,1,0,600,80
*                                                           		   
****************************************************************************/
void ParseFrame(char *DebugFrame)
{
  char *s = strtok(DebugFrame, Delimiter);
  vfs_vol_l = strtok(NULL, ".");
  vfs_vol_ul = strtok(NULL, Delimiter);
  vfs_temp = strtok(NULL, Delimiter);
 // s = strtok(NULL, Delimiter);
  vfs_flow_cnt = strtok(NULL, Delimiter);
 // s = strtok(NULL, Delimiter);
  vfs_start_cnt = strtok(NULL, Delimiter);
 // s = strtok(NULL, Delimiter);
  wmpulse_cnt = strtok(NULL, Delimiter);
 // s = strtok(NULL, Delimiter);
  temp1 = strtok(NULL, Delimiter);
 // s = strtok(NULL, Delimiter);
  batt_v = strtok(NULL, Delimiter);
// DISABLE THIS AFTER DEBUGGING:
/*  Serial.println(vfs_vol_l);
  Serial.println(vfs_vol_ul);
  Serial.println(vfs_temp);
  Serial.println(vfs_flow_cnt);
  Serial.println(vfs_start_cnt);
  Serial.println(wmpulse_cnt);
  Serial.println(temp1);
  Serial.println(batt_v);
*/
}

/***************************************************************************/
