/************************************************************************
*   Function module for generating menu structure on LCD module
*
*   The menu will cycle through the following items:
*     (0) Volume
*     (1) VFS temperature
*     (2) Flow counter
*     (3) Start counter
*     (4) Pulse counter
*     (5) Air temperature
*     (6) Battery voltage
*
*   Morten P. Møller, February 2023
*
************************************************************************/
#ifndef MenuHandler_h
#define MenuHandler_h

/***************************************************************************/
/* parameters */

/***************************************************************************/
/* variables */
extern unsigned char ActiveMenuItem;      // Index for which menu item to show on LCD (0-6)
extern String full, left, right;          // For string operations

/***************************************************************************/
/* function prototypes */
void InitializeMenu(void);
void UpdateStartupMessage(unsigned int ticks);
void PrintBoolean(bool bit);
void ChangeMenuItem(void);
void ToggleReceivedIndicator(bool state);
void UpdateMenu(char *vfs_vol_l, char *vfs_vol_ul, char *vfs_temp, char *vfs_flow_cnt, char *vfs_start_cnt, char *wmpulse_cnt, char *temp1, char *batt_vl);
 
#endif
/***************************************************************************/
