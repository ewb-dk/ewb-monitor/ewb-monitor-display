/***************************************************************************
# Controller for switch output (for power control)
#
# Morten P. Møller, march 2023
#
#****************************************************************************
Revision
2023-03-21 MPM first draft

****************************************************************************/
/* includes */
#include <Arduino.h>
#include "Switch.hpp"
#include "Global.hpp"

/***************************************************************************/
/* parameters */

/***************************************************************************/
/* defines */
int SwitchOutputPort = 2;     // Port mapping, used for power self-holding
int SwitchInputPort = 3;      // Port mapping, used for screen navigation

/***************************************************************************/
/* variables */

/****************************************************************************
* Initialize output pin for switch             		  
*****************************************************************************/
void InitializeSwitch() 
{
  pinMode (SwitchOutputPort, OUTPUT);
  pinMode (SwitchInputPort, INPUT);
}

/****************************************************************************
* Sets the digital output for power self-holding                                            
* Same value as parameter "State"                                      		  
*****************************************************************************/
void SetSwitch(bool State)
 {
   digitalWrite(SwitchOutputPort, State); 
 }
 
 /****************************************************************************
* Returns the digital read of switch input pin                                            
* Return TRUE if HIGH, otherwise FALSE                                      		  
*****************************************************************************/
unsigned char GetSwitch(void)
 {
   return digitalRead(SwitchInputPort); 
 }
/***************************************************************************/
