/***************************************************************************
*   Function module for generating menu structure on LCD module
*
*   The menu will cycle through the following items:
*     > Volume
*     > VFS temperature
*     > Flow counter
*     > Start counter
*     > Pulse counter
*     > Air temperature
*     > Battery voltage
*     > FW info
*
*   Morten P. Møller, February 2023
*
*****************************************************************************
Revision
2023-03-21 MPM first draft

****************************************************************************/
/* includes */
#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <string.h>
#include "MenuHandler.hpp"
#include "Global.hpp"

/***************************************************************************/
/* parameters */

/***************************************************************************/
/* defines */

/***************************************************************************/
/* variables */
unsigned char ActiveMenuItem;       // Index for which menu item to show on LCD (0-6)
String full, left, right;           // For string operations
LiquidCrystal_I2C lcd(0x27,16,2);   // LCD structure

/****************************************************************************
* Initialize function                                                		 
*****************************************************************************/
void InitializeMenu(void)
{
  ActiveMenuItem = 0;       // Start menu index = 0
  lcd.init();
  lcd.clear();
  lcd.backlight();
  
  lcd.setCursor(0,0);       // Show start-up screen
  lcd.print("Loading values");
  lcd.setCursor(0,1);       // Show start-up screen
}

/****************************************************************************
* Update start-up screen while awaiting telegram from Monitor    
* Parameter "ticks" is amount of seconds since boot. Every time, a dot is added.                                            		 
*****************************************************************************/
void UpdateStartupMessage(unsigned int ticks)
{
  if (ticks > 15)           // Too many dots, no communication with Monitor
  {
    lcd.clear();
    lcd.setCursor(0,0);     // Show start-up screen
    lcd.print("Error:");
    lcd.setCursor(0,1);
    lcd.print("Not connected");
  }
  else                      // Add a dot
  {
    lcd.setCursor(ticks,1); // Pick the right spot on LCD
    lcd.print(".");
  }
}


/****************************************************************************
* Print boolean value                                                		 
*****************************************************************************/
void PrintBoolean(bool bit)
{
  lcd.clear();
  lcd.setCursor(0,0); 
  lcd.print("Switch: ");
  if (bit)
    lcd.print("TRUE");
  else
    lcd.print("FALSE");
}

/****************************************************************************
* Update menu item due to switch activation                                                		 
*****************************************************************************/
void ChangeMenuItem(void)
{
  ActiveMenuItem++;
  if (ActiveMenuItem > 7)
    ActiveMenuItem = 0;
}

/****************************************************************************
* Toogle "frame received" symbol based on "state"                                               		 
*****************************************************************************/
void ToggleReceivedIndicator(bool state)
{
  lcd.setCursor(15,1);
  if (state)
    lcd.print("#");
  else
    lcd.print(" "); 
}

/****************************************************************************
* Update menu using the actual selected and newest values            		 
*****************************************************************************/
void UpdateMenu(char *vfs_vol_l, char *vfs_vol_ul, char *vfs_temp, char *vfs_flow_cnt, char *vfs_start_cnt, char *wmpulse_cnt, char *temp1, char *batt_v)
{
  lcd.clear();
  
  switch (ActiveMenuItem)
  {
    case 0:     // Volume  
      lcd.setCursor(0,0);
      lcd.print("Volume:");
      lcd.setCursor(2,1);
      lcd.print(vfs_vol_l);
      lcd.print(".");
      lcd.print(vfs_vol_ul);
      lcd.print(" L");
    break;

    case 1:     // VFS temperature
      lcd.setCursor(0,0);
      lcd.print("VFS temperature:");
      lcd.setCursor(2,1);
      if (vfs_temp != "?")
      {
        full = vfs_temp;
        right = full.substring((full.length()-1), full.length());
        left = full.substring(0, full.length()-1);
        lcd.print(left);
        lcd.print(".");
        lcd.print(right);
      }
      else
        lcd.print(vfs_temp);
      lcd.print(" deg C");
    break;

    case 2:     // Flow counter
      lcd.setCursor(0,0);
      lcd.print("Flow counter:");
      lcd.setCursor(2,1);
      lcd.print(vfs_flow_cnt);
    break;

    case 3:     // Start counter 
      lcd.setCursor(0,0);
      lcd.print("Start counter:");
      lcd.setCursor(2,1);
      lcd.print(vfs_start_cnt);
    break;

    case 4:     // Pulse counter
      lcd.setCursor(0,0);
      lcd.print("Pulse counter:");
      lcd.setCursor(2,1);
      lcd.print(wmpulse_cnt);
    break;

    case 5:     // Air temperature
      lcd.setCursor(0,0);
      lcd.print("Air temperature:");
      lcd.setCursor(2,1);
      if (temp1 != "?")
      {
        full = temp1;
        right = full.substring((full.length()-1), full.length());
        left = full.substring(0, full.length()-1);
        lcd.print(left);
        lcd.print(".");
        lcd.print(right);
      }
      else
        lcd.print(temp1);
      lcd.print(" deg C");
    break;

    case 6:     // Battery voltage
      lcd.setCursor(0,0);
      lcd.print("Battery voltage:");
      lcd.setCursor(2,1);
      if (batt_v != "?")
      {
        full = batt_v;
        right = full.substring((full.length()-1), full.length());
        left = full.substring(0, full.length()-1);
        lcd.print(left);
        lcd.print(".");
        lcd.print(right);
      }
      else
        lcd.print(batt_v);
      lcd.print(" V");
    break;

    case 7:     // FW info 
      lcd.setCursor(0,0);       
      lcd.print("* EWB MONITOR *");
      lcd.setCursor(0,1);
      lcd.print(FW_VERSION);
    break;

    default:
      ActiveMenuItem = 0;
    break;
  }
}
/***************************************************************************/