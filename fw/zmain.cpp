/***************************************************************************
# Display Box for EWB Monitor Firmware
# Board: "Arduino Uno"
# 
# Morten P. Møller, March 2023
#
#****************************************************************************
# Revision
# 2023-03-23 MPM: First drift
# 2023-09-18 MPM: Adaptation to new HW platform (frame format), 115.200 baud
# 2023-10-03 MPM: General improvements
# 
#***************************************************************************/
/* settings and parameters */

/***************************************************************************/
/* includes  */
#include <Arduino.h>
#include <stddef.h>
#include <string.h>
#include "Parser.hpp"
#include "MenuHandler.hpp"
#include "Switch.hpp"
#include "Global.hpp"

/***************************************************************************/
/* defines  */

/***************************************************************************/
/* variables  */
String DebugFrameReceived;        // Complete serial frame received
unsigned int ShutdownTickCtr;     // Used for tracking time for shutdown
unsigned char LastSwitchState;    // Debouncing latch for switch
unsigned char MonitorDetected;    // Flag if any response from Monitor is received

/***************************************************************************/
void setup()
{
  InitializeSwitch();           // Setup switch 
  SetSwitch(1);                 // Set output HIGH for power self-holding
  InitializeMenu();             // Show startup screen
  InitializeParser();           // Initialize parsing function and serial port (debug frames)
  DebugFrameReceived.reserve(50); // Allocate 50 chars
  ShutdownTickCtr = 0; 
  LastSwitchState = 0;
  SendFrameRequest();           // Request values from Monitor
}

/***************************************************************************/
void loop()
{
  /**** GET SERIAL PORT INPUT AND PROCESS IF FRAME READY ****/
  if (ReadCharToStr())                      // If a complete debug frame is received -> start parsing content 
  {
    ToggleReceivedIndicator(1);             // Enable symbol on LCD
    DebugFrameReceived = GetString();   // CHECK: TOO EARLY TO REENABLE RX??   
//    Serial.println("frame!");
//    Serial.print(DebugFrameReceived);
    delay(70);                              // Time where receive indicator is active on LCD       
    ToggleReceivedIndicator(0);             // Disable symbol on LCD
    if (IdentifyFrame())                    // If frame can be accepted
    {
      MonitorDetected = 1;                  // Flag: a correct frame received (values are stored)
      ParseFrame(DebugFrameReceived.c_str()); // Parse frame into sub values, if frame is identified
      UpdateMenu(vfs_vol_l, vfs_vol_ul, vfs_temp, vfs_flow_cnt, vfs_start_cnt, wmpulse_cnt, temp1, batt_v);
    }
    ClearString();                          // Ready for next frame
  }

  /**** CHECK SWITCH PRESS AND UPDATE LCD MENU ITEM ****/
  if (GetSwitch())                          // If button pressed, switch menu screen
  {
    if ((!LastSwitchState) && (MonitorDetected))
    {
      ChangeMenuItem();
      UpdateMenu(vfs_vol_l, vfs_vol_ul, vfs_temp, vfs_flow_cnt, vfs_start_cnt, wmpulse_cnt, temp1, batt_v);
      LastSwitchState = 1;
      ShutdownTickCtr = 0;
    }
  }
  else
    LastSwitchState = 0;

  /**** DURING LOADING PHASE, UPDATE MESSAGE ON LCD ****/
  if ((!MonitorDetected) && (ShutdownTickCtr % 1000 == 0)) // If no message from Monitor recevied, update info
    UpdateStartupMessage(round(ShutdownTickCtr / 1000));
      
  /**** CHECK IF TIME TO AUTO SHUT-DOWN ****/
  if (ShutdownTickCtr > 50000)               // 1ms * 50000 = 50s -> Shutdown
    SetSwitch(0);
  
  delay(1);                                 // 1ms delay -> tick count
  ShutdownTickCtr++;
}

/***************************************************************************/